class Cart{
    products: Item[];

    constructor(){
        this.products= [];
    }

    add(item: Item){
        let found = Object.keys(this.products).filter(key => this.products[key].id == item.id)[0]
            if(found === undefined){
                this.products.push(item);

                var cartRow = document.createElement('div')
                cartRow.classList.add('cart-row')
                cartRow.setAttribute("id","cart-row-"+item.id)
                var cartItems = document.getElementsByClassName('cart-items')[0]
                var cartItemNames = cartItems.getElementsByClassName('cart-item-title')

                cartRow.innerHTML = '<div class="cart-item cart-column"> \
                <img class="cart-item-image" src="' + item.imageUrl + '" width="100" height="100"> \
                <span class="cart-item-title">' + item.name + '</span> \
                 </div>\
                    <span class="cart-price cart-column">' + item.price + '</span>\
                <div class="cart-quantity cart-column">\
                    <input class="cart-quantity-input" type="number" min ="0" value="1">\
                    <button class="btn btn-danger" type="button">USUŃ</button>\
                </div>';

                cartItems.append(cartRow)
                cartRow.getElementsByClassName('btn-danger')[0].addEventListener("click", function(){
                    main.removeItem(item.id);})
                cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener("change", function(){
                    main.updateCartTotal();})
                main.updateCartTotal();
            }
        else{
            alert("Przedmiot już dodany");
        }
    }

    removeItem(id: number){
        let found = Object.keys(this.products).filter(key => this.products[key].id == id)[0]
        if(found !== undefined){
            var element = document.getElementById("cart-row-"+id)
            while (element.firstChild) {
                element.removeChild(element.firstChild);
            }
            element.parentNode.removeChild(element);
        }
        delete this.products[found];
        main.updateCartTotal();
    }
    

    

}

