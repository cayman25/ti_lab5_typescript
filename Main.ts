class Main{
    cart: any;

    constructor(){
        this.getDataFromApi()
    }

    getDataFromApi() {
        this.cart = new Cart();
        let url = 'http://localhost:3000/items';
        fetch(url)
        .then(res => res.json())
        .then((out) => {
    
       for(var i = 0 ; i < out.length ; i++){
            let item = new Item(out[i].id, out[i].name, out[i].price, out[i].imageUrl);
            item.showItem(item);
        }});
    }

    addItemToCart(item: Item){
        this.cart.add(item)
    }

    removeItem(id: number) {
        this.cart.removeItem(id);
    }

    updateCartTotal() {
        var cartItemContainer = document.getElementsByClassName('cart-items')[0]
        var cartRows = cartItemContainer.getElementsByClassName('cart-row')
        var total = 0
        for (var i = 0; i < cartRows.length; i++) {
            var cartRow = cartRows[i]
            var priceElement = cartRow.getElementsByClassName('cart-price')[0]
            var amount = cartRow.getElementsByClassName('cart-quantity-input')[0]
           var price = parseFloat(priceElement.innerText);
           var quantity = amount.value
           total = total + (price * quantity)
        }
        total = Math.round(total * 100) / 100
        document.getElementsByClassName('cart-total-price')[0].innerText = 'PLN' + total
    }
}

let main = new Main()
