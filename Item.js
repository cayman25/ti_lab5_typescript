var Item = /** @class */ (function () {
    function Item(id, name, price, imageUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imageUrl = imageUrl;
    }
    Item.prototype.showItem = function (item) {
        var shopitem = document.createElement("div");
        shopitem.setAttribute("class", "shop-item");
        shopitem.innerHTML = '<span class="shop-item-title">' + item.name + '</span> \
                              <img class="shop-item-image" src="' + item.imageUrl + '"> \
                              <div class="shop-item-details"> \
                              <span class="shop-item-price">' + item.price + 'PLN</span> </div>';
        var button = document.createElement("button");
        button.setAttribute("class", "btn btn-primary shop-item-button");
        button.setAttribute("type", "button");
        button.addEventListener("click", function () {
            main.addItemToCart(item);
        });
        button.innerText = "DODAJ";
        shopitem.appendChild(button);
        document.getElementById("shop-items").appendChild(shopitem);
    };
    return Item;
}());
