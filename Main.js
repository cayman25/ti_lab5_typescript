var Main = /** @class */ (function () {
    function Main() {
        this.getDataFromApi();
    }
    Main.prototype.getDataFromApi = function () {
        this.cart = new Cart();
        var url = 'http://localhost:3000/items';
        fetch(url)
            .then(function (res) { return res.json(); })
            .then(function (out) {
            for (var i = 0; i < out.length; i++) {
                var item = new Item(out[i].id, out[i].name, out[i].price, out[i].imageUrl);
                item.showItem(item);
            }
        });
    };
    Main.prototype.addItemToCart = function (item) {
        this.cart.add(item);
    };
    Main.prototype.removeItem = function (id) {
        this.cart.removeItem(id);
    };
    Main.prototype.updateCartTotal = function () {
        var cartItemContainer = document.getElementsByClassName('cart-items')[0];
        var cartRows = cartItemContainer.getElementsByClassName('cart-row');
        var total = 0;
        for (var i = 0; i < cartRows.length; i++) {
            var cartRow = cartRows[i];
            var priceElement = cartRow.getElementsByClassName('cart-price')[0];
            var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0];
            var price = parseFloat(priceElement.innerText);
            var quantity = quantityElement.value;
            total = total + (price * quantity);
        }
        total = Math.round(total * 100) / 100;
        document.getElementsByClassName('cart-total-price')[0].innerText = 'PLN' + total;
    };
    return Main;
}());
var main = new Main();
